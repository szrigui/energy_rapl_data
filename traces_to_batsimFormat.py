#!/usr/bin/env python3
# basic command to use:  python3 traces_to_batsimFormat.py ./post_processed_dataset.csv ./d_energy_agg_minutes.csv -th 0.5
import argparse 
import pandas as pd
import numpy as np



def discretise_pattern(series):

    pattern_list=[]
    #l=[series[0]]
    total=series[1]# we ignore the first reading
    series=series[1:]
    size=1
    #print(np.mean(l))
    for i in range(len(series)-1):

        if( (np.abs(series[i]-total/size )<0.5) or (size<5) ) :
            #l+=[series[i]]

            total+=series[i]
            size+=1
        else:
            pattern_list.append( [size,total/size ] )
            total=series[i+1]
            size=1
            i+=1

    
    if(size>1):
        pattern_list.append( [size,total/size ] )

        
    m=[]
    for pattern in pattern_list:
        m+=(pattern[0]*[pattern[1]])
    #print(pattern_list)
    #print(len(pattern_list))
    #plt.plot(series)
    #plt.ylim(0,7)
    #plt.plot(m)
    #plt.show() 
    return (pattern_list)

def unsufficant_energy_readings(series):
    return len(series)< 10
        
def log_energy_pattern(identifier,save_path,pattern_list,max_energy,speed):
    with open( save_path ,"w" )as f:
        for i in range(len(pattern_list)):
            nb_cores=round(min(pattern_list[i][1]/max_energy,1),2) # energy_use/max_energy
            nb_instructions=int(round((pattern_list[i][0]*60 * speed)/nb_cores)) #time(min)* processor speed
            f.write( str(identifier)+ " m_usage "+ str(nb_cores) + " "+ str(nb_instructions) +"\n" )
    
def generate_batsim_patterns(df,df_energy,max_energy,proc_size,speed):
    nb_insufficient_RAPL_Observations=0
    nb_not_full_processors=0

    nb_profiled_jobs=0
    for j in range(len (df)):
        #if j>1:
        #    break

        job=df.iloc[j]
        if job["class"]== 'insufficient_RAPL_Observations/':
            print(job["job_id"], ": insufficient number of RAPL Observations ")
            nb_insufficient_RAPL_Observations+=1
            continue
        if job["class"]== 'not_full_processors/':
            print(job["job_id"], ": does not use a full processor ")
            nb_not_full_processors+=1
            continue
        job_id=job["job_id"]
        #print("++++++++++++++++",job_id,"++++++++++++++++")
        start_time=job["start_time_oar"]
        stop_time=job["stop_time_oar"]
        hosts=job["host_list"].split('-')
        processors=job["processor"].split("-")

        profile_ids=[]
        for i in range( len(hosts)):
            d=df_energy[ (df_energy["timestamp_minute"]>=start_time/60) & 
                        (df_energy["timestamp_minute"]<=stop_time/60) &
                        (df_energy["hostname"] == ("dahu"+hosts[i]) ) ] [  ["timestamp_minute",("pp0/package"+processors[i])]   ]
            d=d.rename(columns={("pp0/package"+processors[i]): "energy"})
            series=np.array(d["energy"])
            series=series/10e7
            if unsufficant_energy_readings(series):# some job have partially missing energy reading
                print(len(series))
                print("unsufficant_energy_readings *****")
                continue
            
            profile_id=str(job_id)+"_"+str(hosts[i])+"_"+str(processors[i])+".txt"
            profile_ids.append(profile_id)
            save_path="./o/"+profile_id

            pattern_list=discretise_pattern(series)
            log_energy_pattern(i,save_path,pattern_list,max_energy,speed)
        with open( "./o/"+str(job_id)+".txt" ,"w" )as f:
            for l in profile_ids:
                f.write(l+"\n")
        nb_profiled_jobs+=1
    print("total number of jobs: ",df.shape[0])
    print( "number of jobs with number of RAPL Observations :",nb_insufficient_RAPL_Observations)
    print( "number of jobs that do not use a full processor :",nb_not_full_processors)
    print( "number of jobs profiles created :",nb_profiled_jobs)
          
            







def main():
    parser = argparse.ArgumentParser(description='Reads a job information and RAPL information and transfrom to Bastsim adapted format')
    parser.add_argument('input_job_file', type=str, help='job desciption file')
    parser.add_argument('input_energy_file', type=str, help='machine energy consumption file')
    parser.add_argument('-th', '--threshold',type=float, default=1,help=' hyper-parameter: threshold value to change phase ')
    parser.add_argument('-me', '--max_energy',type=float, default=6,help=' hyper-parameter: estimated max energy a processor can use')
    parser.add_argument('-nbc', '--processor_size',type=float, default=16,help=' hyper-parameter: number of cores per processor')
    parser.add_argument('-s', '--speed',type=float, default=1,help=' hyper-parameter: speed ')

    args=parser.parse_args()


    df=pd.read_csv(args.input_job_file)
    df_energy=pd.read_csv(args.input_energy_file)

    max_energy=args.max_energy
    proc_size=args.processor_size
    speed=args.speed
    generate_batsim_patterns(df,df_energy,max_energy,proc_size,speed)



if __name__ == "__main__":
    main()